import Home from "./views/Home.vue"
import Projects from "./views/Projects.vue";
import NotFound from "./views/NotFound.vue";

/** @type {import('vue-router').RouterOptions['routes']} */
export let routes = [
    {path: "/", component: Home, meta: {title: "Home"}},
    {path: "/projects", component: Projects, meta: {title: "Projects"}},
    {path: "/:path(.*)", component: NotFound},
];
